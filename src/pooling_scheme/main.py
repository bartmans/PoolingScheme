#!/usr/bin/env python

import argparse
import ast
from itertools import combinations

import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist


def get_possible(num_pools, num_repl, seed=None):
    col_idxs = np.array(list(combinations(range(num_pools), num_repl)))

    row_idxs = np.repeat(np.arange(col_idxs.shape[0]).reshape(-1, 1), num_repl, axis=1)

    possible = np.zeros([col_idxs.shape[0], num_pools])
    possible[row_idxs, col_idxs] = 1

    # If need to shuffle all the possible rows
    if seed is not None:
        rng = np.random.default_rng(seed)
        rng.shuffle(possible, axis=0)

    return possible


def get_scheme(
    num_pools,
    num_repl,
    overlap,
    num_drugs=None,
    cap=None,
    seed=None,
    save=None,
    possible=None,
):
    # If list of possible rows is not given as argument, use get_possible
    # function to get all possible rows
    if possible is None:
        possible = get_possible(num_pools, num_repl, seed)

    # Create a list of indicies from possible array that create a new pooling
    # scheme
    idxs = [0]

    for i, row in enumerate(possible):
        # If there is a cap on drugs present per pool, check it first
        if cap is not None and np.any(np.sum(possible[idxs + [i]], axis=0) >= cap):
            continue

        # Make sure new row in pooling scheme does not overlap with previous rows
        dists = cdist(possible[idxs], [row], metric="hamming") * num_pools
        if np.amax(num_repl - dists / 2) <= overlap:
            idxs.append(i)

            # Stop the loop if desired number of rows has been reached
            if num_drugs is not None and len(idxs) >= num_drugs:
                break

    # Convert the pooling scheme from numpy array to pandas DataFrame
    new_scheme = pd.DataFrame(
        possible[idxs],
        columns=[f"Pool_{i}" for i in range(1, num_pools + 1)],
        index=[f"Drug_{i}" for i in range(1, len(idxs) + 1)],
    )

    metadata = {
        "num_pools": num_pools,
        "num_repl": num_repl,
        "overlap": overlap,
        "num_drugs": num_drugs,
        "cap": cap,
        "seed": seed,
    }

    # If save is specified, save to csv
    if save is not None:
        with open(save, "w") as fh:
            fh.write("# " + str(metadata) + "\n")
            new_scheme.to_csv(fh)

    # Return as dataframe
    return new_scheme, metadata


def read_header(filename):
    with open(filename, "r") as fh:
        return ast.literal_eval(fh.readlines(1)[0].strip("# \n"))


def apply_constraints(
    pool_scheme, mz_values, rt_values, mz_threshold=2.0, rt_threshold=0.5
):
    # Get number of pools and number of replicates from the pooling scheme
    num_pools = pool_scheme.shape[1]
    num_repl = pool_scheme.sum(axis=1).unique()
    assert len(num_repl) == 1, "Invalid pooling scheme"
    num_repl = num_repl[0]

    # Make sure that both constraint arrays are of the same length
    assert len(mz_values) == len(rt_values)

    if pool_scheme.shape[0] < len(mz_values):
        raise ValueError(
            f"Too many compounds given ({len(mz_values)}) "
            f"for this pooling scheme ({len(pool_scheme)})"
        )

    unused_scheme_idxs = set(range(pool_scheme.shape[0]))
    idxs = []

    for idx, (mz_val, rt_val) in enumerate(zip(mz_values, rt_values)):
        # Check with which other drugs the current one overlaps in terms of MZ
        # and RT values
        mz_cond = np.isclose(mz_val, mz_values[:idx], rtol=0, atol=mz_threshold)
        rt_cond = np.isclose(rt_val, rt_values[:idx], rtol=0, atol=rt_threshold)
        overlapping_drugs = np.where(mz_cond & rt_cond)[0]

        # variable for holding new index of the pooling scheme.
        # If None at the end of this step of the loop, then
        # the given pooling scheme cannot fit the given list of compounds
        new_ps_idx = None

        # If current drug overlaps with any of the previous ones, then
        if len(overlapping_drugs):
            # Get the rows of pooling scheme that have already been accepted
            # and are mapped to drugs that are overlapping with the drug in
            # current step of the loop
            scheme_part = pool_scheme.iloc[idxs].iloc[overlapping_drugs]

            # Find the index of the pooling scheme that fits the current drug
            for i in unused_scheme_idxs:
                # Calculate the number of dissimilar columns (pools) between
                # the given row of pooling scheme and part of the scheme
                # already built
                num_dissimilar = (
                    cdist(scheme_part, [pool_scheme.iloc[i]], "hamming") * num_pools
                )

                # Check which possible new row has maximum dissimilarity and
                # assign this row as new row in the pooling scheme with
                # additional constraints
                if np.all(num_dissimilar == 2 * num_repl):
                    unused_scheme_idxs.remove(i)
                    new_ps_idx = i
                    break

            # If new index has not been found then throw an error
            if new_ps_idx is None:
                raise RuntimeError(
                    "Pool scheme incompatible with the given constraintes!"
                )
        else:
            new_ps_idx = unused_scheme_idxs.pop()

        idxs.append(new_ps_idx)

    return pool_scheme.iloc[idxs].reset_index(drop=True)


def cli():
    parser = argparse.ArgumentParser(description="Generate a new scheme")

    parser.add_argument("num_pools", type=int, help="Number of pools")
    parser.add_argument("num_repl", type=int, help="Number of replicates")
    parser.add_argument(
        "overlap", type=int, help="Maximum overlap between rows in pooling scheme"
    )
    parser.add_argument("save", help="Filename")
    parser.add_argument("--num_drugs", type=int, help="Number of drugs")
    parser.add_argument(
        "--cap",
        type=int,
        help="Limit on the number of compounds within a single pool",
    )
    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for shuffling the possible rows in pooling scheme",
    )

    args = parser.parse_args()
    get_scheme(**vars(args))
