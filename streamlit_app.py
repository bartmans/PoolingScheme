#!/usr/bin/env python3

import json
import pandas as pd
import streamlit as st

from pooling_scheme import get_scheme as _get_scheme
from pooling_scheme import apply_constraints


@st.cache_data(max_entries=100)
def get_scheme(*args, **kwargs):
    return _get_scheme(*args, **kwargs)


def add_drug_info(pool_scheme):
    # Give the option to upload table with compound names
    # TODO: need to incorporate MZ and RT info
    st.markdown(
        """
        Table with compound names (which is optional) should contain columns in
        the following order:
        1. Names  
        2. MZ (optional)  
        3. RT (optional)  
        """
    )
    compounds = st.file_uploader(
        "Upload table with compounds:",
        type=["csv", "xlsx"],
        key="compounds",
    )

    col1, col2 = st.columns(2)
    with col1:
        mz_thresh = st.number_input("M/Z threshold", value=2.0)
    with col2:
        rt_thresh = st.number_input("RT threshold", value=0.5)

    # Check if a table with compound names was given
    if compounds is not None:
        # Read in the table correctly based on the format
        if compounds.name.endswith(".csv"):
            df_comps = pd.read_csv(compounds)
        elif compounds.name.endswith(".xlsx"):
            df_comps = pd.read_excel(compounds)

        # Make sure that the number of compounds given in the table does not
        # exceed the capacity of the pooling scheme
        num_comps = df_comps.shape[0]
        max_num_comps = pool_scheme.shape[0]

        # If number of compounds exceeds the capacity of the pooling scheme
        # show the message
        if num_comps > max_num_comps:
            st.warning(
                f"Too many compounds given ({num_comps}): "
                "increase number of pools or decrease number of replicates!"
            )
        # Otherwise, add the labels to the pooling scheme
        else:
            # If MZ and RT are given
            if df_comps.shape[1] > 1:
                message = (
                    " Try increasing number of pools or decreasing "
                    "number of replicates or changing the seed."
                )
                try:
                    pool_scheme = apply_constraints(
                        pool_scheme,
                        df_comps.iloc[:, 1],
                        df_comps.iloc[:, 2],
                        mz_thresh,
                        rt_thresh,
                    )
                except ValueError as e:
                    st.warning(str(e) + message)
                except RuntimeError as e:
                    st.warning(str(e) + message)
                else:
                    pool_scheme = df_comps.join(pool_scheme.reset_index(drop=True))
            # If just the names are given
            else:
                pool_scheme = df_comps.join(pool_scheme.reset_index(drop=True))

    return pool_scheme


def main():
    st.title("Create a pooling scheme")

    # Create boxes to input parameters
    col1, col2, col3 = st.columns(3)
    with col1:
        num_pools = st.number_input(
            "Number of pools", 10, key="num_pools", max_value=92
        )
    with col2:
        num_repl = st.number_input(
            "Number of replicates", value=4, min_value=2, key="num_repl"
        )
    with col3:
        overlap = st.number_input("Overlap size", 2, key="overlap")

    # Optional arguments
    st.markdown("**Optional arguments:**")
    col1, col2, col3 = st.columns(3)
    with col1:
        num_drugs = st.number_input(
            "Number of drugs", value=None, step=1, key="num_drugs"
        )
    with col2:
        cap = st.number_input(
            "Cap",
            value=None,
            step=1,
            key="cap",
            help="Maximum number of drugs per pool",
        )
    with col3:
        seed = st.number_input(
            "Seed",
            value=42,
            step=1,
            key="seed",
            help="Random number used in the algorithm",
        )

    # Create the pooling scheme
    pool_scheme, metadata = get_scheme(
        num_pools, num_repl, overlap, num_drugs, cap, seed
    )

    if st.toggle("Additional drug info", value=False, key="add_drug_info"):
        pool_scheme = add_drug_info(pool_scheme)

    # Add warning that num_drugs is bigger than pooling scheme
    if num_drugs is not None and num_drugs > len(pool_scheme):
        st.warning(
            f"Number of drugs parameter ({num_drugs}) is bigger than the maximum size "
            f"of this pooling scheme ({len(pool_scheme)}).\nTry changing number of "
            "pools or seed number."
        )

    st.write(pool_scheme)

    st.write(
        "Number of drugs per pool:",
        pool_scheme.filter(like="Pool")
        .sum()
        .rename(index="Number of drugs")
        .to_frame()
        .transpose(),
    )

    st.download_button(
        label="Download data",
        data=(f"# {metadata}\n" + pool_scheme.to_csv()).encode("utf-8"),
        file_name="pooling_scheme.csv",
        mime="text/csv",
    )


if __name__ == "__main__":
    main()
